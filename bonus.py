import sys
from old_solar import (
    get_observer_location,
    get_sun_position,
    print_position,
    detailed_print,
    get_cli_args,
    get_location_from_lat_long,
)
from ipaddress import ip_address


def main():
    parser = get_cli_args()
    args = parser.parse_args()

    if "" != args.ip:
        try:
            test = ip_address(args.ip)
            print(test)
        except ValueError as err:
            sys.exit(f"Please try again: {err}")

    if "" in args.ip and args.position == [39.0438, -77.4874]:
        try:
            lat, long = get_observer_location(args.ip)
        except KeyError as err:
            print(f"Not a valid Public IP address: {args.ip}")
            sys.exit(1)

        body_info = get_sun_position(args.body, [lat, long])

    elif args.position != [39.0438, -77.4874]:
        get_location_from_lat_long(args.position)
        body_info = get_sun_position(args.body, args.position)

    print_position(body_info)
    detailed_print(body_info)


if __name__ == "__main__":
    main()


# DEMO EXAMPLE COMMANDS
# python3 old_solar.py sun
# python3 old_solar.py orion
# python3 old_solar.py --ip 147.179.80.1 pluto
# python3 old_solar.py --ip 50.35.77.33 jupiter
# python3 old_solar.py --position 34.6937 -92.3629 sun
# python3 old_solar.py --position 42.9789771 -70.949501 sun
