#!/usr/bin/env python3
import requests
import click
import config
from datetime import datetime
from requests.auth import HTTPBasicAuth
from pprint import pprint


# ASTRONOMYAPI_ID= "1af92c1b-77e4-4863-8bc8-33aa8764c36e"
# ASTRONOMYAPI_SECRET="6ddb2910787e1161daac95abeaaa08295bee3234f18a0fb0fe7f1f58f5684742f484dfa49a2dcf4705a57c1c211cf2a68aa75bb491fdb9dae347d8bb1c75a791b0828fc4c9052842b22d0775ed299fe6ccdf567836d7f3e0221d78832275a8b2d0df05935544f6bfa439e402c7536331"


def get_observer_location(ip=""):
    """Returns the longitude and latitude for the location of this machine.
    Returns:
    str: latitude
    str: longitude"""
    # https://api.zip-codes.com/ZipCodesAPI.svc/1.0/GetZipCodeDetails/20149?key=DEMOAPIKEY
    url = f"http://ip-api.com/json/{ip}"
    response = requests.get(url)

    location = response.json()
    lat = location["lat"]
    lon = location["lon"]
    # print(location)
    time = datetime.now()
    current_time = time.strftime("%H:%M %p")
    current_date = time.strftime("%B %d, %Y")
    print(
        f"From {location['city']}, {location['region']} at {current_time} on {current_date}:"
    )
    # print(lat, lon)
    return lat, lon


def get_sun_position(latitude, longitude, body="sun"):
    """Returns the current position of the sun in the sky at the specified location
    Parameters:
    latitude (str)
    longitude (str)
    Returns:
    float: azimuth
    float: altitude
    """
    time = datetime.now()
    current_date = time.strftime("%Y-%m-%d")
    current_time = time.strftime("%H:%M:%S")

    params = {
        "latitude": latitude,
        "longitude": longitude,
        "elevation": 295,
        "from_date": current_date,
        "to_date": current_date,
        "time": current_time,
    }

    url = f"https://api.astronomyapi.com/api/v2/bodies/positions/{body}"
    new_url = f"https://api.astronomyapi.com/api/v2/bodies/positions/{body}?latitude={latitude}&longitude={longitude}&from_date={current_date}&to_date={current_date}&time={current_time}/sun"
    response = requests.get(
        url,
        auth=HTTPBasicAuth(config.ASTRONOMYAPI_ID, config.ASTRONOMYAPI_SECRET),
        params=params,
    )

    stuff = response.json()
    # print(stuff['data']['table']['rows'][0]['cells'][0]['extraInfo']['magnitude'])
    horizonal = stuff["data"]["table"]["rows"][0]["cells"][0]["position"]["horizonal"]

    body_info = {
        "altitude": horizonal["altitude"]["degrees"],
        "azimuth": horizonal["azimuth"]["degrees"],
        "distance": stuff["data"]["table"]["rows"][0]["cells"][0]["distance"][
            "fromEarth"
        ]["km"],
        "magnitude": stuff["data"]["table"]["rows"][0]["cells"][0]["extraInfo"][
            "magnitude"
        ],
    }

    # print(altitude, azimuth)
    # pprint(stuff['data']['table']['rows'][0]['cells'][0])
    # NOTE: Replace with your real return values!
    return body_info


def print_position(body_info):
    """Prints the position of the sun in the sky using the supplied coordinates
    Parameters:
    body_info = {
    azimuth (float),
    altitude (float)
    }"""

    print(
        f"The Sun is currently at: {body_info['azimuth']} degrees azimuth, {body_info['altitude']} degrees altitude\n"
    )


def detailed_print(body_info):
    """
    Sun:
        Distance from Earth: 149,597,900 km
        Magnitude: -26.74
        Position:
        Azimuth: 45 deg 12 min 47 sec
        Altitude: 10 deg 15 min 22 sec
    :return:
    """
    print("Sun:")
    print(f'  Distance from Earth: {body_info["distance"]} km')
    print(f'  Magnitude: {body_info["magnitude"]}')
    print("  Position:")
    print(f'    Azimuth: {body_info["azimuth"]}')
    print(f'    Altitude: {body_info["altitude"]}')
    pass


if __name__ == "__main__":
    latitude, longitude = get_observer_location()
    body_info = get_sun_position(latitude, longitude)
    print_position(body_info)
    detailed_print(body_info)
