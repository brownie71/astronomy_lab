#!/usr/bin/env python3
import requests
import argparse
import config
import sys
from ipaddress import ip_address
from datetime import datetime
from requests.auth import HTTPBasicAuth


def get_observer_location(ip=""):
    """Returns the longitude and latitude for the location of this machine.
    Returns:
    str: latitude
    str: longitude"""

    url = f"http://ip-api.com/json/{ip}"

    response = requests.get(url)

    location = response.json()

    latitude = location["lat"]
    longitude = location["lon"]

    date, time = get_date_time()
    print(
        f"From {location['city']}, {location['region']} at {time} on {date}:\n"
    )

    return latitude, longitude


def get_sun_position(body, *args):
    """Returns the current position of the sun in the sky at the specified location
    Parameters:
    body (str)
    latitude (args: tuple)
    longitude (args: tuple)
    Returns:
    body_info = {
        'name': string,
        'altitude': float,
        'azimuth': float,
        'altitude_degrees': string,
        'azimuth_degrees': string,
        'distance': integer,
        'magnitude': float,
    }
    """
    latitude, longitude = args[0]

    time = datetime.now()
    date = time.strftime("%Y-%m-%d")
    time = time.strftime("%H:%M:%S")

    params = {
        "latitude": latitude,
        "longitude": longitude,
        "elevation": 295,
        "from_date": date,
        "to_date": date,
        "time": time,
    }

    url = f"https://api.astronomyapi.com/api/v2/bodies/positions/{body}"

    response = requests.get(
        url,
        auth=HTTPBasicAuth(config.ASTRONOMYAPI_ID, config.ASTRONOMYAPI_SECRET),
        params=params,
    )

    stuff = response.json()

    horizonal = stuff["data"]["table"]["rows"][0]["cells"][0]["position"][
        "horizonal"
    ]

    body_info = {
        "name": stuff["data"]["table"]["rows"][0]["cells"][0]["name"],
        "altitude": horizonal["altitude"]["string"],
        "azimuth": horizonal["azimuth"]["string"],
        "altitude_degrees": horizonal["altitude"]["degrees"],
        "azimuth_degrees": horizonal["azimuth"]["degrees"],
        "distance": stuff["data"]["table"]["rows"][0]["cells"][0]["distance"][
            "fromEarth"
        ]["km"],
        "magnitude": stuff["data"]["table"]["rows"][0]["cells"][0][
            "extraInfo"
        ]["magnitude"],
    }

    return body_info


def get_location_from_lat_long(*args):
    location_url = "https://vanitysoft-boundaries-io-v1.p.rapidapi.com/reaperfire/rest/v1/public/boundary/zipcode/location"

    querystring = {
        "latitude": str(args[0][0]),
        "longitude": str(args[0][1]),
        "radius": "1",
    }

    headers = {
        "x-rapidapi-key": "4cbc00dba0msh9f58292ed3aca33p16641cjsn9fe4afe51a70",
        "x-rapidapi-host": "vanitysoft-boundaries-io-v1.p.rapidapi.com",
    }

    response = requests.get(location_url, headers=headers, params=querystring)
    location = response.json()

    if location.get("features", []):
        city = location["features"][0]["properties"]["city"]
        state = location["features"][0]["properties"]["state"]
    else:
        sys.exit(f"Please enter a valid [LATITUDE LONGITUDE]")

    date, time = get_date_time()
    print(f"From {city}, {state} at {time} on {date}:\n")
    return city, state


def print_position(body_info):
    """Prints the position of the sun in the sky using the supplied coordinates
    Parameters:
    body_info = {
    azimuth (float),
    altitude (float)
    }"""

    print(
        f"The {body_info['name']} is currently at: {body_info['azimuth_degrees']} degrees azimuth, {body_info['altitude_degrees']} degrees altitude"
    )


def detailed_print(body_info):
    """Sun:
        Distance from Earth: 149,597,900 km
        Magnitude: -26.74
        Position:
        Azimuth: 45 deg 12 min 47 sec
        Altitude: 10 deg 15 min 22 sec
    :return:
    """
    pre_azimuth = body_info["azimuth"].split()
    azimuth = {
        "degrees": pre_azimuth[0].strip("\xb0"),
        "min": pre_azimuth[1].strip("'"),
        "seconds": pre_azimuth[2].strip('"'),
    }

    pre_altitude = body_info["altitude"].split()
    altitude = {
        "degrees": pre_altitude[0].strip("\xb0"),
        "min": pre_altitude[1].strip("'"),
        "seconds": pre_altitude[2].strip('"'),
    }

    pre_distance = str(body_info["distance"])
    distance = pre_distance.split(".")

    print("-" * 50)
    print(body_info["name"])
    print("  Distance from Earth: {:,} km".format(int(distance[0])))
    print(f'  Magnitude: {body_info["magnitude"]}')
    print("  Position:")
    print(
        f'    Azimuth: {azimuth["degrees"]} deg {azimuth["min"]} min {azimuth["seconds"]} sec'
    )
    print(
        f'    Altitude: {altitude["degrees"]} deg {altitude["min"]} min {altitude["seconds"]} sec'
    )
    print(("-" * 50), "\n")


def get_date_time():
    time = datetime.now()
    current_time = time.strftime("%H:%M %p")
    current_date = time.strftime("%B %d, %Y")
    print(current_date, current_time)
    return current_date, current_time


def get_cli_args():
    parser = argparse.ArgumentParser()
    # OPTIONAL ARGUMENTS
    parser.add_argument(
        "--ip", default="", help="Current Public IP address < 147.179.80.1 >"
    )
    parser.add_argument(
        "--at", help="Start Date Year-Month-Day < 2021-04-01T20:00:00 >"
    )
    parser.add_argument(
        "--position",
        nargs=2,
        default=[39.0438, -77.4874],
        help="Latitude longitude [39.0438, -77.4874]",
    )
    # REQUIRED ARGUMENT
    parser.add_argument(
        "body",
        choices=[
            "moon",
            "mercury",
            "venus",
            "earth",
            "mars",
            "jupiter",
            "saturn",
            "uranus",
            "neptune",
            "pluto",
            "sun",
        ],
        help="Name of the planet",
    )

    return parser


if __name__ == "__main__":
    parser = get_cli_args()
    args = parser.parse_args()

    if "" != args.ip:
        try:
            test = ip_address(args.ip)
            print(test)
        except ValueError as err:
            sys.exit(f"Please try again: {err}")

    if "" in args.ip and args.position == [39.0438, -77.4874]:
        try:
            lat, long = get_observer_location(args.ip)
        except KeyError as err:
            print(f"Not a valid Public IP address: {args.ip}")
            sys.exit(1)

        body_info = get_sun_position(args.body, [lat, long])

    elif args.position != [39.0438, -77.4874]:
        get_location_from_lat_long(args.position)
        body_info = get_sun_position(args.body, args.position)

    print_position(body_info)
    detailed_print(body_info)


# DEMO EXAMPLE COMMANDS
# python3 old_solar.py sun
# python3 old_solar.py orion
# python3 old_solar.py --ip 147.179.80.1 pluto
# python3 old_solar.py --ip 50.35.77.33 jupiter
# python3 old_solar.py --position 34.6937 -92.3629 sun
# python3 old_solar.py --position 42.9789771 -70.949501 sun
